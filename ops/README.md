# OPS

## Exercises made by Alwin Rodewijk - 635653

These exercises are make for the leason Operating systems by Alwin Rodewijk.

## Exercise 1: Command-line parameters

Status: Finished

## Exercise 2: Syntax check and multitasking

Status: Finished

## Exercise 3: Forking tasks using fork() and wait()

Status: Finished
Note: compile syntaxCheck.o yourself using ex02/syntaxCheck.c

## Exercise 4: Forking different tasks using execl()

Status: Finished
Note: compile syntaxCheck.o yourself using ex02/syntaxCheck.c