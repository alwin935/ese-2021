#define _XOPEN_SOURCE 500
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <signal.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>

// General functions
void printSeperator(void);
void errorCheck(int x, const char *y);

// Thread functions
void *ThreadFunction(void *arg);
void multiThreading(void);

// Pipe functions
void pipeTest(void);
void pipeComplex(void);

// FIFO functions
void FIFO_Test(void);
void FIFO_Read(void);
void FIFO_Write(void);
const char FIFO_File[] = "FIFO";

// Shared memory functions
void sharedMemory(void);
void makeSharedMemory(void);
void readSharedMemory(void);
const char shmFile[] = "/MY_SHM";

int main(void)
{
   printSeperator();

   multiThreading();

   pipeTest();

   pipeComplex();

   FIFO_Test();

   sharedMemory();

   return 0;
}

void sharedMemory()
{
   printf("This is a test with shared memory.\n");
   makeSharedMemory();
   readSharedMemory();

   printSeperator();
}

void makeSharedMemory()
{
   char message[] = "This is my shared memory message.";
   int fd;

   fd = shm_open(shmFile, O_RDWR | O_CREAT, 0600);
   errorCheck(fd, "shm_open");
   errorCheck(ftruncate(fd, sizeof(message)), "ftruncate");
   char *shmAddr = mmap(NULL, sizeof(message), PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
   errorCheck(shmAddr == MAP_FAILED, "mmap");
   errorCheck(close(fd), "close");
   memcpy(shmAddr, message, sizeof(message));

   errorCheck(munmap(shmAddr, sizeof(message)), "munmap");
}

void readSharedMemory()
{
   int fd;
   struct stat statBuf;
   char *shmAddr;

   fd = shm_open(shmFile, O_RDONLY, 0);
   errorCheck(fd, "R-shm_open");
   errorCheck(fstat(fd, &statBuf), "R-fstat");

   shmAddr = mmap(NULL, statBuf.st_size, PROT_READ, MAP_SHARED, fd, 0);
   errorCheck(shmAddr == MAP_FAILED, "R-mmap");

   close(fd);

   printf("Message from SHM: %s\n", shmAddr);

   errorCheck(munmap(shmAddr, statBuf.st_size), "munmap");

   errorCheck(shm_unlink(shmFile), "shm_unlink");
}

void FIFO_Test()
{
   printf("This is a test with FIFO.\n");
   switch (fork())
   {
   case -1:
      perror("fork");
      break;

   case 0: // Child
      FIFO_Read();
      exit(EXIT_SUCCESS);
      break;

   default: // Parent
      FIFO_Write();
      break;
   }
   wait(NULL);
   printSeperator();
}

void FIFO_Write()
{
   int fd;
   char message[] = "This is my FIFO message.";

   errorCheck(mkfifo(FIFO_File, S_IFIFO | 0666), "mkfifo");
   fd = open(FIFO_File, O_WRONLY);
   errorCheck(fd, "open");
   errorCheck(write(fd, message, sizeof(message)), "write");
   errorCheck(close(fd), "close");
   unlink(FIFO_File);
}

void FIFO_Read()
{
   int fd;
   char message[30] = "Nothing is set.";

   fd = open(FIFO_File, O_RDWR);
   read(fd, message, sizeof(message));
   close(fd);
   printf("Message from FIFO: %s\n", message);
}

void pipeComplex()
{
   printf("This is a test with more complex pipes.\n");
   int pipeFD[2];
   char buf, buffer[20];

   if (pipe(pipeFD) == -1)
   {
      perror("pipe");
   }

   switch (fork())
   {
   case -1:
      perror("fork");
      break;

   case 0: // Child
      close(pipeFD[0]);
      dup2(pipeFD[1], 1); // Writing on pipeFD[1]
      printf("PING!");
      close(pipeFD[1]);
      exit(EXIT_SUCCESS);
      break;

   default: // Parent
   {
      int i = 0;
      close(pipeFD[1]);

      while (read(pipeFD[0], &buf, 1) > 0)
      {
         buffer[i] = buf;
         i++;
      }
      buffer[i] = '\0';
      printf("Message from child: %s\nMessage from pipe: %i\n", buffer, pipeFD[1]);
      wait(NULL);

      break;
   }
   }
   printSeperator();
}

void pipeTest()
{
   printf("This is a test with pipes.\n");

   char message[10];
   int pipeFD[2];

   if (pipe(pipeFD) == -1)
   {
      perror(" Pipe: ");
   }

   switch (fork())
   {
   case -1: // Error
      perror("Fork:");
      break;

   case 0: // Child
      printf("Parent ID: %i\t Child ID: %i\n", getppid(), getpid());
      close(pipeFD[1]);
      read(pipeFD[0], message, 6);
      close(pipeFD[0]);
      printf("Message from pipe: %s\n", message);
      exit(EXIT_SUCCESS);
      break;

   default: // Parent
      close(pipeFD[0]);
      write(pipeFD[1], "Help!", 6);
      close(pipeFD[1]);
      wait(NULL);
      break;
   }
   printSeperator();
}

void multiThreading()
{
   printf("This is a test with multi threading.\n");

   pthread_t ThreadID_A, ThreadID_B;

   const int ThreadArgA = 6, ThreadArgB = 9;
   int *pResultA, *pResultB;

   if (pthread_create(&ThreadID_A, NULL, ThreadFunction, (void *)&ThreadArgA))
   {
      fprintf(stderr, "No thread A.\n");
   }

   if (pthread_create(&ThreadID_B, NULL, ThreadFunction, (void *)&ThreadArgB))
   {
      fprintf(stderr, "No thread B.\n");
   }

   printf("Thread A has ID: %li.\n", ThreadID_A);
   printf("Thread B has ID: %li.\n", ThreadID_B);

   pthread_join(ThreadID_A, (void **)&pResultA);
   pthread_join(ThreadID_B, (void **)&pResultB);

   printf("Original A: %i \t Result A: %i\n", ThreadArgA, *pResultA);
   printf("Original B: %i \t Result B: %i\n", ThreadArgB, *pResultB);

   printSeperator();
}

void *ThreadFunction(void *arg)
{
   int *pResult = malloc(sizeof(int));
   *pResult = *(int *)arg;
   *pResult *= 10;
   pthread_exit((void *)pResult);
}

void printSeperator()
{
   printf("------------------------------------------------------\n");
}

void errorCheck(int x, const char *y)
{
   if (x < 0)
   {
      perror(y);
   }
}
