/******************************************************************************
 * File:         getsignal.c
 * Date:         2021-01-03
 * Author:       Alwin Rodewijk
 * Description:  OPS exercise 5: signals
 ******************************************************************************/

#include <stdio.h>    // stdout
#include <sys/types.h>  // getpid()
#include <unistd.h>     // write(), sleep(), getpid()
#include <signal.h>     // sigaction()
#include <string.h>     // memset()

void signalAction(int sig);

static char number = '0';

int main(void)
{
    struct sigaction actionSettings, oldActionSettings;
    memset(&actionSettings, '\0', sizeof(actionSettings));
    actionSettings.sa_handler = signalAction;
    actionSettings.sa_flags = 0;
    sigemptyset(&actionSettings.sa_mask);

    if(sigaction(25, &actionSettings, &oldActionSettings))
    {
        printf("\t--\tFailed sigaction()\t--\n");
        return 0;
    }

    printf("PID: %i\n", getpid());

    fflush(stdout);
    while (1)
    {
        write(1, &number, sizeof(number));
        sleep(1);
    }

    return 0;
}

void signalAction(int sig)
{
    if (number >= '9')
    {
        number = '0';
    }
    else
    {
        number++;
    }
}