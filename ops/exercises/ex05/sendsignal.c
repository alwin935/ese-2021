/******************************************************************************
 * File:         sendsignal.c
 * Date:         2021-01-03
 * Author:       Alwin Rodewijk
 * Description:  OPS exercise 5: signals
 ******************************************************************************/

#include <stdio.h>          // stdout
#include <stdlib.h>         // system()
#include <unistd.h>         // write(), sleep(), getpid()
#include <string.h>

char sendSignal[15] = "kill -25 ";

int main(int argc, char *argv[])
{
    if (argc <= 1)
    {
        printf("Correct syntax:\n./sendsignal <PID of getsignal>\n");
        exit(EXIT_FAILURE);
    }
    
    strcat(sendSignal, argv[1]);
    printf("Sending signals using command: %s.\n", sendSignal);

    while (1)
    {
        system(sendSignal);
        sleep(3);
    }
    return 0;
}
