# Why execl("./filter", "filter", NULL) is correct?

The first string is the executable, the second string is arg0 which is the name of the executable. Following arg0 is arg1 and so forth ending with 'NULL' as a terminator.
