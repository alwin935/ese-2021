/******************************************************************************
 * File:         redirect.c
 * Date:         2021-01-04
 * Author:       Alwin Rodewijk
 * Description:  OPS exercise 7: pipes
 ******************************************************************************/

#include <stdio.h>    // stdout, exit()
#include <unistd.h>   // read(), write(), fork()
#include <stdlib.h>     // 
#include <sys/wait.h> // wait()

#define READ_SIDE 0
#define WRITE_SIDE 1
#define ESC 0x1B

const int testCharActive = 0;

int main(void)
{
    // Setup unnamed pipes
    int pipeFD0[2];
    int pipeFD1[2];
    if (pipe(pipeFD0) == -1 || pipe(pipeFD1) == -1)
    {
        perror("pipe");
        exit(1);
    }

    // fork
    int pidChild = fork();
    switch (pidChild)
    {
    // Error
    case -1:
        perror("fork");
        exit(1);
        break;

    // Child
    case 0:
        close(pipeFD0[WRITE_SIDE]);
        close(pipeFD1[READ_SIDE]);
        
        dup2(pipeFD0[READ_SIDE], 0);  // redirect stdin to pipeFD0[0] and close stdin
        dup2(pipeFD1[WRITE_SIDE], 1); // redirect stdout to pipeFD1[1] and close stdout
        
        execl("./filter", "filter", NULL);
        break;

    // Parent
    default:
    {
        close(pipeFD0[READ_SIDE]);
        close(pipeFD1[WRITE_SIDE]);
        char buf0;
        char buf1;
        char testChar = testCharActive ? '-' : '\0';
        
        while (waitpid(pidChild, NULL, WNOHANG) == 0)
        {
            read(0, &buf0, sizeof(buf0));
            write(pipeFD0[WRITE_SIDE], &buf0, sizeof(buf0));

            if(buf0 == ESC){
                printf("\n");
                exit(1);
            }

            read(pipeFD1[READ_SIDE], &buf1, sizeof(buf1));
            write(1, &buf1, sizeof(buf1));
            write(1, &testChar, 1);
        }
    }
        break;
    }
    return 0;
}
