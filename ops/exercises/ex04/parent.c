/******************************************************************************
 * File:         display.c
 * Version:      1.4
 * Date:         2018-02-20
 * Author:       M. van der Sluys, J.H.L. Onokiewicz, R.B.A. Elsinghorst, J.G. Rouland
 * Description:  OPS exercise 2: syntax check
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/resource.h>
#include "displayFunctions.h"

#define START_INDEX_CHAR 4

int main(int argc, char *argv[])
{
  ErrCode err;
  int iChild, niceIncr;
  u_int8_t isParrent;

  err = SyntaxCheck(argc, argv); // Check the command-line parameters
  if (err != NO_ERR)
  {
    DisplayError(err); // Print an error message
  }
  else
  {
    niceIncr = strtoul(argv[3], NULL, 10); // String to unsigned long
    iChild = argc - START_INDEX_CHAR;
    printf("Index\tNice\tChar\n");
    for (int i = 0; i < iChild; i++)
    {
      int forkRetVal = fork();

      switch (forkRetVal)
      {
      case -1: // Error
        printf("Fork failed at index %i.\n", i);
        break;

      case 0: // Child proces
      {
        // Set nice value of child
        char reniceCommand[30];
        sprintf(reniceCommand, "renice -n %i -p %i > /dev/null", i * niceIncr, getpid());
        system(reniceCommand);

        // Print information
        printf("%i \t %i \t %c \n", i, getpriority(PRIO_PROCESS, getpid()), argv[START_INDEX_CHAR + i][0]);

        // Start ex02/display
        char *dirDisplay = "../ex02/display";
        char *display = "display";
        char *printType = argv[1];
        char *printTimes = argv[2];
        char *printChar = &argv[START_INDEX_CHAR + i][0];
        int execlRetVal = execl(dirDisplay, display, printType, printTimes, printChar, (char *)NULL);

        if (execlRetVal == -1)
        {
          perror("Error:");
        }

        i = iChild;
        isParrent = 0;
      }
      break;

      default: // Parent proces
        isParrent = 1;
        break;
      }
    }

    // Wait for child processes to complete
    if (isParrent == 1)
    {
      for (int i = 0; i < iChild; i++)
      {
        wait(NULL);
      }
      printf("All children have finished.\n");
    }
  }
  return 0;
}
