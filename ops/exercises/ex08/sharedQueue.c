/******************************************************************************
 * File:         sharedQueue.c
 * Version:      1.2
 * Date:         2021-01-04
 * Author:       J. Onokiewicz, M. van der Sluys
 * Description:  OPS exercise 8: Multithreading
 ******************************************************************************/

#include "Queue.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <signal.h>
#include <stdbool.h>
#include <string.h>
#include <dirent.h>
#include <errno.h>

void *writeToQueue(void *arg);
void *writeToFile(void *unused);
void quitingApp(int unused);

const char filePath[] = "file/export%i.txt";

pthread_mutex_t lockQueue;
queue_t queue = {NULL};
int queueSize = 0;
bool quitNow = false;

pthread_mutex_t lockQuiting;
bool quiting = false;

int main()
{
  // Cleanup file folder
  DIR *dir = opendir("file");
  if (dir)
  {
    closedir(dir);
    system("rm -r file/*");
  }
  else if (ENOENT == errno)
  {
    system("mkdir file");
  }
  else
  {
    printf("Checking ./file dir failed.\n");
  }

  // Setup signal ctrl+c to set quiting to true
  struct sigaction actionSettings, oldActionSettings;
  memset(&actionSettings, '\0', sizeof(actionSettings));
  actionSettings.sa_handler = quitingApp;
  actionSettings.sa_flags = 0;
  sigemptyset(&actionSettings.sa_mask);

  if (sigaction(SIGINT, &actionSettings, &oldActionSettings))
  {
    printf("SIGINT was not set.\n");
    exit(EXIT_FAILURE);
  }

  // Create mutex
  if (pthread_mutex_init(&lockQuiting, NULL) != 0)
  {
    printf("Mutex quiting init failed\n");
    exit(EXIT_FAILURE);
  }
  if (pthread_mutex_init(&lockQueue, NULL) != 0)
  {
    printf("Mutex queue init failed\n");
    exit(EXIT_FAILURE);
  }

  pthread_t ThreadID_A, ThreadID_B, ThreadID_C, ThreadID_D;

  const int ThreadArgA = 2, ThreadArgB = 3, ThreadArgC = 4;

  if (pthread_create(&ThreadID_A, NULL, writeToQueue, (void *)&ThreadArgA))
  {
    fprintf(stderr, "No thread A.\n");
  }

  if (pthread_create(&ThreadID_B, NULL, writeToQueue, (void *)&ThreadArgB))
  {
    fprintf(stderr, "No thread B.\n");
  }

  if (pthread_create(&ThreadID_C, NULL, writeToQueue, (void *)&ThreadArgC))
  {
    fprintf(stderr, "No thread C.\n");
  }

  if (pthread_create(&ThreadID_D, NULL, writeToFile, NULL))
  {
    fprintf(stderr, "No thread D.\n");
  }

  pthread_join(ThreadID_A, NULL);
  pthread_join(ThreadID_B, NULL);
  pthread_join(ThreadID_C, NULL);
  pthread_join(ThreadID_D, NULL);

  return 0;
}

void *writeToFile(void* unused)
{
  (void)unused;   // variable is unused
  int fileNumber = 0;
  
  while (true)
  {
    pthread_mutex_lock(&lockQueue);
    if (queueSize >= 15)
    {
      printf("Writing the 'Data' shown below to file: ./file/export%i\n", fileNumber);
      showQueue(&queue);
      printf("\n");

      char currentFilePath[50];
      FILE *currentFile;
      data_t *currentData;

      sprintf(currentFilePath, filePath, fileNumber);
      currentFile = fopen(currentFilePath, "w+");

      if (currentFile == NULL)
      {
        printf("Can't create file.\n");
        deleteQueue(&queue);
      }
      else
      {
        for (int i = 0; i < queueSize; i++)
        {
          currentData = frontQueue(&queue);
          //printf("Writing to file: %i - %s\n", currentData->intVal, currentData->text);
          fprintf(currentFile, "%i - %s\n", currentData->intVal, currentData->text);
          popQueue(&queue);
        }
        fclose(currentFile);
      }

      fileNumber++;

      queueSize = 0;

      pthread_mutex_lock(&lockQuiting);
      if (quiting)
      {
        quitNow = true;
        pthread_mutex_unlock(&lockQuiting);
        pthread_mutex_unlock(&lockQueue);
        break;
      }
      pthread_mutex_unlock(&lockQuiting);
    }
    pthread_mutex_unlock(&lockQueue);
  }

  pthread_exit(NULL);
}

void *writeToQueue(void *arg)
{
  data_t data;
  data.intVal = *(int *)arg;
  sprintf(data.text, "Thread nr %i", data.intVal);

  while (true)
  {
    sleep(data.intVal);
    pthread_mutex_lock(&lockQueue);

    if (quitNow)
    {
      pthread_mutex_unlock(&lockQueue);
      break;
    }

    // write to queue
    if (queueSize == 0)
    {
      createQueue(&queue, data);
    }
    else
    {
      pushQueue(&queue, data);
    }
    queueSize++;

    pthread_mutex_unlock(&lockQueue);
  }

  pthread_exit(NULL);
}

void quitingApp(int unused)
{
  (void)unused;   // variable is unused
  pthread_mutex_lock(&lockQuiting);
  quiting = true;
  pthread_mutex_unlock(&lockQuiting);
  printf("\nStopping application\n\n");
}
