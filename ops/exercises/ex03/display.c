/******************************************************************************
 * File:         display.c
 * Version:      1.4
 * Date:         2018-02-20
 * Author:       M. van der Sluys, J.H.L. Onokiewicz, R.B.A. Elsinghorst, J.G. Rouland
 * Description:  OPS exercise 2: syntax check
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/time.h>
#include <sys/resource.h>
#include "displayFunctions.h"

#define START_INDEX_CHAR 4

int main(int argc, char *argv[])
{
  unsigned long int numOfTimes;
  char printMethod, printChar;
  ErrCode err;
  int iChild, niceIncr;

  err = SyntaxCheck(argc, argv); // Check the command-line parameters
  if (err != NO_ERR)
  {
    DisplayError(err); // Print an error message
  }
  else
  {
    printMethod = argv[1][0];
    numOfTimes = strtoul(argv[2], NULL, 10); // String to unsigned long
    niceIncr = strtoul(argv[3], NULL, 10);   // String to unsigned long
    iChild = argc - START_INDEX_CHAR;

    for (int i = 0; i < iChild; i++)
    {
      int forkRetVal = fork();

      switch (forkRetVal)
      {
      case -1: // Error
        printf("Fork failed at index %i.\n", i);
        break;

      case 0: // Child proces
      {
        // Set nice value of child
        char reniceCommand[30];
        sprintf(reniceCommand, "renice -n %i -p %i > /dev/null", i * niceIncr, getpid());
        system(reniceCommand);

        printChar = argv[START_INDEX_CHAR + i][0];

        // Print information
        printf("%i \t %i \t %c \n", i, getpriority(PRIO_PROCESS, getpid()), argv[START_INDEX_CHAR + i][0]);
        PrintCharacters(printMethod, numOfTimes, printChar); // Print character printChar numOfTimes times using method printMethod
        exit(EXIT_SUCCESS);
      }
      break;

      default: // Parent proces
        break;
      }
    }

    // Wait for child processes to complete

    for (int i = 0; i < iChild; i++)
    {
      wait(NULL);
    }

    printf("\n");
  }

  return 0;
}
