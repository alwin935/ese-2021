/******************************************************************************
 * File:         sendsignal_getpid.c
 * Date:         2021-01-03
 * Author:       Alwin Rodewijk
 * Description:  OPS exercise 6: FIFOs
 ******************************************************************************/

#include <stdio.h>      // stdout
#include <stdlib.h>     // system()
#include <unistd.h>     // write(), sleep(), getpid()
#include <string.h>
#include <fcntl.h>      // open()

#define PID_CHAR_SIZE 6

char sendSignal[15] = "kill -25 ";
const char pipeName[] = "PIDpipe";

int main()
{
    // Get PID from FIFO
    char pid[PID_CHAR_SIZE];
    int fd = open(pipeName, O_RDONLY);
    read(fd, &pid, sizeof(pid));
    close(fd);
    strcat(sendSignal, pid);

    printf("Sending signals using command: %s.\n", sendSignal);

    while (1)
    {
        system(sendSignal);
        sleep(3);
    }
    return 0;
}