/******************************************************************************
 * File:         getsignal_sendpid.c
 * Date:         2021-01-03
 * Author:       Alwin Rodewijk
 * Description:  OPS exercise 6: FIFOs
 ******************************************************************************/

#include <stdio.h>      // stdout
#include <sys/types.h>  // getpid()
#include <unistd.h>     // write(), sleep(), getpid()
#include <signal.h>     // sigaction()
#include <string.h>     // memset()
#include <sys/stat.h>   // mkfifo()
#include <fcntl.h>      // open()
#include <stdlib.h>

#define PID_CHAR_SIZE 6

void signalAction(int sig);

static char number = '0';
const char pipeName[] = "PIDpipe";

int main(void)
{
    // Setup signal action
    struct sigaction actionSettings, oldActionSettings;
    memset(&actionSettings, '\0', sizeof(actionSettings));
    actionSettings.sa_handler = signalAction;
    actionSettings.sa_flags = 0;
    sigemptyset(&actionSettings.sa_mask);

    if(sigaction(25, &actionSettings, &oldActionSettings))
    {
        printf("\t--\tFailed sigaction()\t--\n");
        exit(1);
    }

    // Setup FIFO for PID
    char pid[PID_CHAR_SIZE];
    int fd = 0;
    sprintf(pid, "%i", getpid());
    mkfifo(pipeName, S_IFIFO|0666);
    fd = open(pipeName,O_WRONLY);
    write(fd, pid, sizeof(pid));
    close(fd);
    unlink(pipeName);
    
    // Write number to stdout
    fflush(stdout);
    while (1)
    {
        write(1, &number, sizeof(number));
        sleep(1);
    }

    return 0;
}

void signalAction(int sig)
{
    if (number >= '9')
    {
        number = '0';
    }
    else
    {
        number++;
    }
}