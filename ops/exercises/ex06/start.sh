#!/bin/bash
# File: start.sh

# check if application exists, if not compile it
if ! [ -e ./getsignal_sendpid ] || ! [ -e ./sendsignal_getpid ]; then
    printf "Compiling application..\n"
    gnome-terminal -- bash -c "make; exit; exec bash" & compilePID1=$!
    wait $compilePID1
fi

gnome-terminal -- ./getsignal_sendpid
gnome-terminal -- ./sendsignal_getpid
