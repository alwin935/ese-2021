#include "test.h"
#include <iostream>
#include <array>
#include <algorithm>
#include <cmath>
#include <map>
#include <vector>
#include <utility>
#include <functional>

void mapFunction();

void argMap(int argc, char *argv[]);
typedef std::map<std::string, std::function<float(float, float)>> functionMap;
float multipy(float x, float y) { return x * y; }
float add(float x, float y) { return x + y; }
float suptract(float x, float y) { return x - y; }
float devide(float x, float y) { return (y == 0.0) ? 0 : (x / y); }

void lambdaFunction();

int main(int argc, char *argv[])
{
   test printer;
   printer.printSeperator();
   lambdaFunction();

   printer.printSeperator();
   argMap(argc, argv);

   printer.printSeperator();
   mapFunction();

   printer.printSeperator();
   return 0;
}

void lambdaFunction()
{
   std::vector<int> testArray{3, 8, 5, 9, 6, 9, 4, 7, 8, 9};

   std::cout << "Current array:\n";
   std::for_each(testArray.begin(), testArray.end(), [](int i) {
      std::cout << i << "  ";
   });
   std::cout << "\n";

   std::sort(testArray.begin(), testArray.end(), [](int x, int y) {
      return x < y;
   });

   std::cout << "Sorted array:\n";
   std::for_each(testArray.begin(), testArray.end(), [](int i) {
      std::cout << i << "  ";
   });
   std::cout << "\n";

   auto p = std::unique(testArray.begin(), testArray.end(), [](int x, int y){
      return x == y;
   });
   testArray.resize(std::distance(testArray.begin(), p));

   std::cout << "Unique array:\n";
   std::for_each(testArray.begin(), testArray.end(), [](int i) {
      std::cout << i << "  ";
   });
   std::cout << "\n";
}

void argMap(int argc, char *argv[])
{
   functionMap argMap;
   argMap.emplace("m", &multipy);
   argMap["a"] = add;
   argMap["s"] = suptract;
   argMap["d"] = devide;

   argMap["multiply"] = multipy;
   argMap["add"] = add;
   argMap["suptract"] = suptract;
   argMap["devide"] = devide;

   std::cout << "The following arguments are valid:\n";
   auto lastCommand = argMap.size();
   for (size_t j = 1; const auto &i : argMap)
   {
      std::cout << i.first;
      if (j == lastCommand)
      {
         std::cout << " ";
      }
      else
      {
         std::cout << ", ";
      }
      j++;
   }
   std::cout << "\n";

   if (argc >= 3 && argMap.find(const_cast<char *>(argv[1])) != argMap.end())
   {
      auto fnc = argMap[const_cast<char *>(argv[1])];
      float result = fnc(atof(const_cast<char *>(argv[2])), atof(const_cast<char *>(argv[3])));
      std::cout << "The argument '" << argv[1] << "' is used.\n";
      std::cout << "The result = " << result << "\n";
   }
}

void mapFunction()
{
   std::cout << "Test with the map function.\n";
   std::map<std::string, double> testMap0;
   testMap0["temperature1"] = 48.18;
   testMap0["temperature2"] = 4.4185;
   testMap0["temperature3"] = -4.44;
   testMap0["temperature4"] = 4.458;

   for (const auto &i : testMap0)
   {
      std::cout << "Identifier: " << i.first << " value: " << i.second << std::endl;
   }

   std::pair<std::string, double> testPair;
   testPair.first = "pairTest";
   testPair.second = 4118.5723;

   testMap0.insert(testPair);

   for (const auto &i : testMap0)
   {
      std::cout << "Identifier: " << i.first << " value: " << i.second << std::endl;
   }

   auto multiply = [](float x, float y) { return x * y; };
}
