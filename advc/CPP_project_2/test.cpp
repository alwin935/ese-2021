#include "test.h"

using namespace std;

test::test() :
   _seperatorMessage{""},
   _data{0}
{
   //_seperatorMessage();
   for (int i = 0; i < width; i++)
   {
      _seperatorMessage.append("-");
   }
   _seperatorMessage.append("\n");
}

test::test(int data) :
   _seperatorMessage{},
   _data{data}
{
   _data = data;
   for (int i = 0; i < width; i++)
   {
      _seperatorMessage.append("-");
   }
   _seperatorMessage.append("\n");
}

test::~test()
{
}

void test::printSeperator()
{
   cout << endl
        << _seperatorMessage;
}

void test::printCentered(string message)
{
   int sizeMessage = static_cast<int>(message.size());
   string centeredMessage;

   for (int i = 0; i < ((width - sizeMessage) / 2); i++)
   {
      centeredMessage.append("-");
   }
   centeredMessage.append(message);
   for (int i = (width + sizeMessage) / 2; i < width; i++)
   {
      centeredMessage.append("-");
   }

   cout << endl
        << centeredMessage << endl
        << endl;
}
