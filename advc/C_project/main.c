#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <complex.h>

#define SEPERATOR_LENGTH 125

// General :)
void printSeperator(void);
void printCenter(char *message, size_t size);

// Pointer functions :)
void pointers(void);
void pointersArray(void);
void HEXdump(void);

// Boolean and command line functions
void booleans(void);
void commandLine(int argc, char *argv[]);

// Complex numbers functions
void complexNumbers(void);

// Qsort functions
void qsortFunction(void);
int compareInts(const void *x, const void *y);

// SLL functions
typedef struct SLLnode
{
   int data;
   struct SLLnode *pNextNode;
} SLLnode_t;
void SLLfunction(void);
void SLLadd(SLLnode_t **ppHead, int data);
int SLLgetData(SLLnode_t **ppHead, int position);
void SLLfree(SLLnode_t **ppHead);

// Compare float functions
void compareFloatFunction(void);
bool compareFloat(float a, float b);
const float epsilon = 0.000001f;

// Void, generic and function pointer
void pointerFunctinon(void);
int pointerFunctinonAdd(int a, int b);
int pointerFunctinonMultiply(int a, int b);

int main(int argc, char *argv[])
{
   printf("\n");
   pointers();
   HEXdump();
   booleans();
   commandLine(argc, argv);
   complexNumbers();
   qsortFunction();
   SLLfunction();
   compareFloatFunction();
   pointerFunctinon();

   printSeperator();
   return 0;
}

void pointerFunctinon()
{
   char messA[] = "Pointer functions";
   printCenter(messA, sizeof(messA));

   int (*pFunction)(int, int) = pointerFunctinonAdd;
   int x = 684;
   int y = 9740;
   printf("Add function points to: %p, *pFunction points to: %p\n", (void *)pointerFunctinonAdd, (void *)pFunction);
   printf("x = %i \t y = %i\n", x, y);
   printf("Result with add: %i\nResult with pFunction: %i\n", pointerFunctinonAdd(x, y), pFunction(x, y));
}

int pointerFunctinonAdd(int a, int b)
{
   return a + b;
}

void compareFloatFunction()
{
   char messA[] = "Compare floats";
   printCenter(messA, sizeof(messA));

   float x = 5.489318;
   float y = 5.47983218;
   printf("When comparing %.8f and %.8f the output is %i using epsilon %.8f.\n", x, y, compareFloat(x, y), epsilon);

   x = 8.489318;
   y = 8.4893185;
   printf("When comparing %.8f and %.8f the output is %i using epsilon %.8f.\n", x, y, compareFloat(x, y), epsilon);
}

bool compareFloat(float a, float b)
{
   return fabs(a - b) < epsilon;
}

void SLLfunction()
{
   char messA[] = "Singly linked list";
   printCenter(messA, sizeof(messA));

   SLLnode_t *pHead = NULL;
   printf("The current head points to: %p.\n", (void *)pHead);
   for (int i = 0; i < 5; i++)
   {
      SLLadd(&pHead, i);
   }

   printf("With 5 values added the head points to: %p and has these values:\n", (void *)pHead);
   int i;
   for (i = 0; i < (5 - 1); i++)
   {
      printf("%d, ", SLLgetData(&pHead, i));
   }
   printf("%d\n", SLLgetData(&pHead, i));

   SLLfree(&pHead);
   printf("The singly linked list has been cleared, head points to: %p\n", (void *)pHead);
}

void SLLfree(SLLnode_t **ppHead)
{
   SLLnode_t *pNext = *ppHead;
   SLLnode_t *pToDelete;

   if (*ppHead != NULL)
   {
      while (1)
      {
         if (pNext->pNextNode != NULL)
         {
            pToDelete = pNext;
            pNext = pNext->pNextNode;
            free(pToDelete);
         }
         else
         {
            free(pNext);
            break;
         }
      }

      *ppHead = NULL;
   }
}

int SLLgetData(SLLnode_t **ppHead, int position)
{
   SLLnode_t *pNext = *ppHead;
   if (pNext == NULL)
   {
      return 0;
   }
   else
   {
      for (int i = 0; i < position; i++)
      {
         pNext = pNext->pNextNode;
      }
      return pNext->data;
   }
}

void SLLadd(SLLnode_t **ppHead, int data)
{
   SLLnode_t *pNext = *ppHead;
   SLLnode_t *pNew = (SLLnode_t *)malloc(sizeof(SLLnode_t));
   if (pNew != NULL)
   {
      pNew->pNextNode = NULL;
      pNew->data = data;
      if (pNext == NULL)
      {
         *ppHead = pNew;
      }
      else
      {
         while (1)
         {
            if (pNext->pNextNode == NULL)
            {
               break;
            }
            else
            {
               pNext = pNext->pNextNode;
            }
         }
         pNext->pNextNode = pNew;
      }
   }
   else
   {
      printf("Malloc failed.. ");
   }
}

void qsortFunction()
{
   char messA[] = "Qsort";
   printCenter(messA, sizeof(messA));

   int data1[10] = {8, 52, 71, 31, 46, 89, 13, 71, 81, 73};
   printf("The current dataset contains: ");
   int i;
   for (i = 0; i < (int)(sizeof(data1) / sizeof(int) - 1); i++)
   {
      printf("%i, ", data1[i]);
   }
   printf("%i.\n", data1[i]);

   qsort(data1, sizeof(data1) / sizeof(int), sizeof(int), compareInts);
   printf("The sorted dataset contains: ");
   for (i = 0; i < (int)(sizeof(data1) / sizeof(int) - 1); i++)
   {
      printf("%i, ", data1[i]);
   }
   printf("%i.\n", data1[i]);
}

int compareInts(const void *A, const void *B)
{
   int a = *(int *)A;
   int b = *(int *)B;

   if (a > b)
   {
      return 1;
   }
   if (b > a)
   {
      return -1;
   }
   return 0;
}

void complexNumbers()
{
   char messA[] = "Complex numbers";
   printCenter(messA, sizeof(messA));

   double complex cA = 1.897 + 8.87 * I;
   double complex cB = 84.5476 + 65.468 * I;
   double complex cC;

   printf("Complex number a = %.2f + %.2f*i, complex number b = %.2f + %.2f*i.\n", creal(cA), cimag(cA), creal(cB), cimag(cB));

   cC = conj(cA);
   printf("The conjugate a = %.2f + %.2f*i.\n", creal(cC), cimag(cC));

   cC = cA + cB;
   printf("a + b = %.2f + %.2f*i.\n", creal(cC), cimag(cC));

   cC = cA - cB;
   printf("a - b = %.2f + %.2f*i.\n", creal(cC), cimag(cC));

   cC = cA * cB;
   printf("a * b = %.2f + %.2f*i.\n", creal(cC), cimag(cC));

   cC = cA / cB;
   printf("a / b = %.2f + %.2f*i.\n", creal(cC), cimag(cC));
}

void commandLine(int argc, char *argv[])
{
   char messA[] = "Command line";
   printCenter(messA, sizeof(messA));

   if (argc == 0)
   {
      printf("There are no command line parameters entered.\n");
   }
   else
   {
      for (int i = 1; i < argc; i++)
      {
         printf("Parameter %i: %s\n", i, argv[i]);
      }
   }
}

void booleans()
{
   char messA[] = "Boolean";
   printCenter(messA, sizeof(messA));

   bool a = true;
   bool b = false;

   printf("A boolean value can have be `%s` = a and `%s` = b.\n", a ? "true" : "false", b ? "true" : "false");

   if (a | b)
   {
      printf("The statement `a | b` returns true.\n");
   }
}

void HEXdump()
{
   char messA[] = "HEX dump";
   printCenter(messA, sizeof(messA));

   int a = 463;
   const unsigned char *pA = (const unsigned char *)&a;
   printf("This is a HEX dump of an int with the value %i = ", a);

   for (int i = 0; i < (int)sizeof(a); i++)
   {
      printf("%02X ", pA[i]);
   }
   printf("\n");

   double b[5] = {345.4, 34.465, 385.48, 34.6481, 3857.46811};
   const unsigned char *pB = (const unsigned char *)b;
   printf("The array: ");
   int ij;
   for (ij = 0; ij < (int)(sizeof(b) / sizeof(b[0]) - 1); ij++)
   {
      printf("%02f, ", b[ij]);
   }
   printf("%02f\n", b[ij]);

   printf("Has the HEX dump:\n");
   for (int i = 0; i < (int)(sizeof(b) / sizeof(b[0])); i++)
   {

      for (int j = 0; j < (int)(sizeof(b[0])); j++)
      {
         printf("%02X ", pB[j]);
      }
      printf("\n");
      pB++;
   }
}

void pointers()
{
   char messA[] = "Pointers";
   printCenter(messA, sizeof(messA));

   int a = 5;
   int *pA = &a;
   printf("Variable int a = %i and is stored in pointer int *pA = %p which points to a value of %i.\n", a, (void *)pA, *pA);

   char b[] = {"Hallo World!"};
   char *pB = b;
   printf("String b = `%s` since a string is a char pointer when char *pB = b = `%s`.\n", b, pB);
   printf(" With write and the size of b the string can be printed. Write is a bit quicker than printf..\n");
   write(1, pB, sizeof(b));

   pointersArray();
}

void pointersArray()
{
   char messA[] = "Pointer array";
   printCenter(messA, sizeof(messA));

   int c[2][4] = {
       {2, 5, 8, 6},
       {8, 4, 5, 7}};
   int *pC = (int *)c;
   printf("Int array c has the adress %p the values:\n", (void *)c);
   for (int i = 0; i < 2; i++)
   {
      for (int j = 0; j < 4; j++)
      {
         printf("%i ", c[i][j]);
      }
      printf("\n");
   }
   printf("Doing this with a pointer (%p) requires the total size of the array:\n", (void *)pC);
   for (int i = 0; i < (int)(sizeof(c) / sizeof(int)); i++)
   {
      printf("%i ", *pC);
      pC++;

      if ((i % 4) == 3)
      {
         printf("\n");
      }
   }
}

void printCenter(char *message, size_t size)
{
   char *pMessage = message;
   int sizeMessage = (int)size - 1;
   char toPrint[SEPERATOR_LENGTH + 1];

   int i;
   for (i = 0; i < SEPERATOR_LENGTH; i++)
   {
      toPrint[i] = '-';
   }
   toPrint[SEPERATOR_LENGTH] = '\0';

   for (i = ((SEPERATOR_LENGTH - sizeMessage) / 2); i < ((SEPERATOR_LENGTH + sizeMessage) / 2); i++)
   {
      toPrint[i] = *pMessage;
      pMessage++;
   }

   printf("\n%s\n\n", toPrint);
}

void printSeperator()
{
   char seperator[SEPERATOR_LENGTH + 1];
   for (int i = 0; i < SEPERATOR_LENGTH; i++)
   {
      seperator[i] = '-';
   }
   seperator[SEPERATOR_LENGTH] = '\0';

   printf("\n%s\n", seperator);
}
