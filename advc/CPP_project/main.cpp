#include "test.h"
#include <iostream>
#include <array>

using namespace std;

// Call by reference function
void callByReference(void);
void swapInt(int &a, int &b);

// Array functies
void arrayFunction(void);

// Class pointer
void pointerClass(void);

int main()
{
   test printer;
   printer.printCentered("Call by reference");
   callByReference();

   printer.printCentered("Array");
   arrayFunction();

   printer.printCentered("Pointer class");
   pointerClass();

   printer.printSeperator();
   return 0;
}

void pointerClass()
{
   test test1(488);
   test *pTest = &test1;

   cout << "Using a pointer to point to a class initialised with 488 returns the value: " << pTest->getData() << endl;

   array<test, 3> myArray = {(4612), (46851), (65498)};

   cout << "Now a array of classes returns these values: ";

   for (auto &i : myArray)
   {
      cout << i.getData() << " ";
   }
   cout << endl;
}

void arrayFunction()
{
   array<int, 5> myArray = {1234, 9852, 2343, 345, 8};

   cout << "The following array has a size of " << myArray.size() << " the values: ";
   for (const auto &i : myArray)
   {
      cout << i << " ";
   }
   cout << endl;
}

void callByReference()
{
   int a = 345, b = 1234;
   cout << "The value a = " << a << " and b = " << b << " are swapt by reference." << endl;
   swapInt(a, b);
   cout << "Now a = " << a << " and b = " << b << "." << endl;
}

void swapInt(int &a, int &b)
{
   int temp = a;
   a = b;
   b = temp;
}