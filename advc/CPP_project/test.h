#ifndef TEST_H
#define TEST_H

#include <iostream>

class test
{
private:
   const int width = 100;
   int _data = 0;
   std::string seperatorMessage;

public:
   test();
   test(int data);
   ~test();

   void printSeperator(void);
   void printCentered(std::string message);

   int getData(void) { return _data; };
   void setData(int i) { _data = i; };
};

#endif
